<?php
return [
    'host' => '0.0.0.0',
    'port' => 9501,
    'mode' => SWOOLE_PROCESS,
    'type' => SWOOLE_SOCK_TCP,
    'setting' => [],
    'process' => [
    ],
    'psr-4' => [
        'App\\Web\\Controllers' => ROOT . '/src/Web/Controllers'
    ]
];
