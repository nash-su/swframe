<?php
return [
    'psr-4' => [
        'SWFrame\\Commands' => ROOT . '/lib/Commands',
        'App\\Commands' => ROOT . '/src/Commands',
    ]
];
