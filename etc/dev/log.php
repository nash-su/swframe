<?php
return [
    'system' => [
        'handler' => SWFrame\LoggerHandler\FileHandler::class,
        'init' => [
            ROOT . '/var/log/system.log'
        ]
    ]
];
