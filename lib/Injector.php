<?php

namespace SWFrame;

use Auryn\ConfigException;
use Auryn\InjectionException;
use Auryn\Reflector;

final class Injector extends \Auryn\Injector
{
    /**
     * @var Injector
     */
    public static Injector $_instance;

    /**
     * @return static
     */
    public static function instance(): self
    {
        empty(self::$_instance) && self::$_instance = new Injector;
        return self::$_instance;
    }

    /**
     * @param Reflector|null $reflector
     * @throws ConfigException
     */
    private function __construct(Reflector $reflector = null)
    {
        parent::__construct($reflector);
        $this->share($this);
    }

    /**
     * @param $callableOrMethodStr
     * @param array $args
     * @return mixed
     * @throws InjectionException
     */
    public function execute($callableOrMethodStr, array $args = array()): mixed
    {
        return parent::execute($callableOrMethodStr, $this->makeParams($args));
    }

    /**
     * @param $name
     * @param array $args
     * @return Injector
     */
    public function define($name, array $args): Injector
    {
        return parent::define($name, $this->makeParams($args));
    }

    /**
     * @param $name
     * @param array $args
     * @return mixed
     * @throws InjectionException
     */
    public function make($name, array $args = array()): mixed
    {
        return parent::make($name, $this->makeParams($args));
    }

    /**
     * @param array $args
     * @return array
     */
    private function makeParams(array $args): array
    {
        $params = [];
        foreach ($args as $k => $v) {
            $params[":$k"] = $v;
        }
        return $params;
    }
}
