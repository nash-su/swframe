<?php

namespace SWFrame;

class Command
{
    /**
     * @param string $className
     * @param string $methodName
     * @param string $fileName
     * @return ApplicationCommand
     */
    public static function makeCommand(string $className, string $methodName, string $fileName):ApplicationCommand
    {
        return new ApplicationCommand(self::makeCommandName($fileName, $methodName), [$className, $methodName]);
    }


    /**
     * @param string $fileName
     * @param string $methodName
     * @return string
     */
    private static function makeCommandName(string $fileName, string $methodName): string
    {
        $commandName = lcfirst($fileName) . ':' . lcfirst($methodName);
        $commandName = preg_replace('/([A-Z])/', '-${1}', $commandName);
        return strtolower($commandName);
    }
}
