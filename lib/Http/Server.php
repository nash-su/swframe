<?php

namespace SWFrame\Http;

use Auryn\ConfigException;
use ReflectionException;
use SWFrame\Http\Mvc\{Request, Response};
use SWFrame\Injector;
use Swoole\{Event as SwEvent, Process, Timer, Http\Server as HttpServer};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process as SymfonyProcess;

class Server
{
    /**
     * @var array
     */
    private array $config = [
        'host' => '0.0.0.0',
        'port' => 9501,
        'mode' => SWOOLE_PROCESS,
        'type' => SWOOLE_SOCK_TCP,
        'setting' => [],
        'process' => [],
        'psr-4' => []
    ];

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @var HttpServer
     */
    private HttpServer $server;

    /**
     * @var Server
     */
    private static Server $_instance;

    /**
     * @param array $config
     */
    private function __construct(array $config)
    {
        $this->config = array_merge($this->config, $config);
        $this->server = new HttpServer(
            $this->config['host'], $this->config['port'],
            $this->config['mode'], $this->config['type']
        );
    }

    /**
     * @param array $config
     * @return void
     */
    public static function init(array $config = []): void
    {
        empty(self::$_instance) && self::$_instance = new static($config);
    }

    /**
     * @return Server
     */
    public static function instance(): Server
    {
        return self::$_instance;
    }

    /**
     * @return void
     * @throws ConfigException
     * @throws ReflectionException
     */
    public function readyInjector(): void
    {
        Injector::instance()->delegate(Request::class, [Request::class, 'instance']);
        Injector::instance()->delegate(Response::class, [Response::class, 'instance']);
        Router::init($this->config['psr-4']);
    }

    /**
     * @return void
     */
    private function setting()
    {
        empty($this->config['setting']) || $this->server->set($this->config['setting']);
    }

    /**
     * @return void
     */
    private function bindEvent()
    {
        $this->server->on('Start', [Event::class, 'onStart']);
        $this->server->on('Request', [Event::class, 'onRequest']);
    }

    /**
     * @return void
     */
    private function addProcessList(): void
    {
        foreach ($this->config['process'] as $processConf) {
            if (is_callable($processConf) || is_string($processConf))
                $this->addProcess('* * * * * *', $processConf);
            elseif (is_array($processConf))
                isset($processConf[1])
                    ? $this->addProcess(...$processConf)
                    : $this->addProcess('* * * * * *', ...$processConf);
        }
    }

    /**
     * @param string $cronFormat
     * @param callable|string $callable
     * @return void
     */
    private function addProcess(string $cronFormat, callable|string $callable): void
    {
        is_callable($callable)
            ? $this->addCallableProcess($cronFormat, $callable)
            : $this->addCommandProcess($cronFormat, $callable);
    }

    /**
     * @param string $cronFormat
     * @param callable $callable
     * @return void
     */
    private function addCallableProcess(string $cronFormat, callable $callable)
    {
        $this->server->addProcess(new Process(function () use ($cronFormat, $callable) {
            $cronExpression = new CronExpression($cronFormat);
            Timer::tick(1000, function (int $timerId, CronExpression $cronExpression, callable $callable) {
                if ($cronExpression->isDue())
                    Injector::instance()->execute($callable);
            }, $cronExpression, $callable);
            SwEvent::wait();
        }));
    }

    /**
     * @param string $cronFormat
     * @param string $processConf
     * @return void
     */
    private function addCommandProcess(string $cronFormat, string $processConf)
    {
        $this->server->addProcess(new Process(function () use ($cronFormat, $processConf) {
            $cronExpression = new CronExpression($cronFormat);
            Timer::tick(1000, function (int $timerId, CronExpression $cronExpression, string $processConf) {
                if ($cronExpression->isDue())
                    (new SymfonyProcess(explode(' ', trim($processConf))))->run(function ($type, $buffer) {
                        Injector::instance()->make(OutputInterface::class)->write($buffer);
                    });
            }, $cronExpression, $processConf);
            SwEvent::wait();
        }));
    }

    /**
     * @return bool
     * @throws ConfigException
     * @throws ReflectionException
     */
    public function start(): bool
    {
        $this->readyInjector();
        $this->setting();
        $this->bindEvent();
        $this->addProcessList();
        return $this->server->start();
    }
}
