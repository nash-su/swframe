<?php

namespace SWFrame\Http\Attributes;

use Attribute;
use FastRoute\RouteCollector;

#[Attribute(Attribute::TARGET_METHOD|Attribute::IS_REPEATABLE)]
class Route
{
    /**
     * @param string|array $method
     * @param string $routeUrl
     */
    public function __construct(private string|array $method, private string $routeUrl)
    {}

    /**
     * @param RouteCollector $routeCollector
     * @param array $callable
     * @return void
     */
    public function __invoke(RouteCollector $routeCollector, array $callable)
    {
        $routeCollector->addRoute($this->method, $this->routeUrl, $callable);
    }
}
