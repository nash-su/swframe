<?php

namespace SWFrame\Http;

use Cron\CronExpression as Expression;
use DateTime;

class CronExpression
{
    private Expression $secExpression, $minExpression;

    /**
     * @param string $format
     */
    public function __construct(string $format)
    {
        $formatArray = explode(' ', trim($format));
        $sec = array_shift($formatArray);
        $min = array_shift($formatArray);
        $this->secExpression = new Expression(implode(' ', [$sec, ...$formatArray]));
        $this->minExpression = new Expression(implode(' ', [$min, ...$formatArray]));
    }

    /**
     * @return bool
     */
    public function isDue(): bool
    {
        if ($this->minExpression->isDue($currentTime = new DateTime())) {
            $currentTime->setTime((int)$currentTime->format('H'), (int)$currentTime->format('s'), 0);
            return $this->secExpression->isDue($currentTime);
        }
        return false;
    }
}
