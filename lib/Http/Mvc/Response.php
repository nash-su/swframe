<?php

namespace SWFrame\Http\Mvc;

use Swoole\Coroutine;
use Swoole\Http\Response as SwResponse;

class Response extends \Symfony\Component\HttpFoundation\Response
{

    /**
     * @param SwResponse $swResponse
     */
    private function __construct(private SwResponse $swResponse)
    {
        parent::__construct();
        $context = Coroutine::getContext();
        $context[Response::class] = $this;
    }

    /**
     * @param SwResponse $response
     * @return static
     */
    public static function createFromSWResponse(SwResponse $response): static
    {
        return new static($response);
    }

    /**
     * @return $this
     */
    public function send(): static
    {
        $this->sendHeaders();
        $this->sendContent();
        return $this;
    }

    /**
     * @return $this
     */
    public function sendContent(): static
    {
        $this->swResponse->end($this->getContent());
        return $this;
    }

    /**
     * @return $this
     */
    public function sendHeaders(): static
    {
        $this->swResponse->setStatusCode($this->getStatusCode());
        foreach ($this->headers->allPreserveCase() as $key => $valList)
            foreach ($valList as $valItem)
                $this->swResponse->header($key, $valItem);
        return $this;
    }

    /**
     * @return static
     */
    public static function instance(): self
    {
        return Coroutine::getContext()[Response::class];
    }
}
