<?php

namespace SWFrame\Http\Mvc;

use SWFrame\Storage\{Database, DatabaseProxy};
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable, ArrayAccess, IteratorAggregate, Traversable, ArrayIterator;

abstract class Model implements JsonSerializable, ArrayAccess, IteratorAggregate
{
    protected static array $tableMap = [];

    /**
     * @return string
     */
    public static function getPoolName(): string
    {
        return 'default';
    }

    /**
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return 'id';
    }

    /**
     * @return Database|DatabaseProxy
     */
    public static function getDatabase(): Database|DatabaseProxy
    {
        return Database::instance(static::getPoolName());
    }

    /**
     * @return string
     */
    public static function getTableName(): string
    {
        if (empty(static::$tableMap[static::class])) {
            $classPath = explode('\\', static::class);
            $className = lcfirst(array_pop($classPath));
            static::$tableMap[static::class] = strtolower(preg_replace('/([A-Z])/', '_${1}', $className));
        }
        return static::$tableMap[static::class];
    }

    /**
     * @param int|string|array $options
     * @param array $order
     * @return static|null
     * @example
     *  Model::get(1)
     *  Model::get('12')
     *  Model::get(['id' => 1])
     *  Model::get(['age[>]' => 13], ['id' => 'DESC'])
     */
    public static function get(int|string|array $options, array $order = []): ?static
    {
        is_array($options) || $options = [static::getPrimaryKey() => $options];
        empty($order) || $options['ORDER'] = $order;
        if ($data = static::getDatabase()->get(static::getTableName(), '*', $options))
            return new static($data);
        return null;
    }

    /**
     * @param int|string|array $options
     * @param array $order
     * @param array|int $limit
     * @return ModelSet|null
     * @example
     *  Model::search(['id' => 1])
     *  Model::search(['age[>]' => 13], ['id' => 'DESC'])
     *  Model::search(['age[>]' => 13], ['id' => 'DESC'], [0, 20])
     */
    public static function search(int|string|array $options, array $order = [], array|int $limit = []): ?ModelSet
    {
        is_array($options) || $options = [static::getPrimaryKey() => $options];
        empty($order) || $options['ORDER'] = $order;
        empty($limit) || $options['LIMIT'] = $limit;
        $set = static::getDatabase()->select(static::getTableName(), '*', $options);
        foreach ($set as $index => $item)
            $set[$index] = new static($item);
        return new ModelSet($set, static::class);
    }

    /**
     * @param int|string|array $options
     * @param array|int $limit
     * @return int
     */
    public static function count(int|string|array $options, array|int $limit = []): int
    {
        is_array($options) || $options = [static::getPrimaryKey() => $options];
        empty($limit) || $options['LIMIT'] = $limit;
        return static::getDatabase()->count(static::getTableName(), $options);
    }

    /**
     * @param int|string|array $options
     * @param array $order
     * @param array|int $limit
     * @return array
     */
    #[ArrayShape(['total' => "int", 'list' => "null|ModelSet"])]
    public static function pagination(int|string|array $options, array $order = [], array|int $limit = []): array
    {
        return [
            'total' => static::count($options),
            'list' => static::search($options, $order, $limit)
        ];
    }

    /**
     * @param array $options
     * @param array|int $limit
     * @return void
     */
    public static function delete(array $options, array|int $limit = []): void
    {
        empty($limit) || $options['LIMIT'] = $limit;
        static::getDatabase()->delete(static::getTableName(), $options);
    }

    /**
     * @param int|string|array $options
     * @param array $rawData
     * @param array|int $limit
     * @return void
     */
    public static function update(int|string|array $options, array $rawData, array|int $limit = []): void
    {
        is_array($options) || $options = [static::getPrimaryKey() => $options];
        empty($limit) || $options['LIMIT'] = $limit;
        static::getDatabase()->update(static::getTableName(), $rawData, $options);
    }

    /**
     * @return static
     */
    public function save(): static
    {
        if (isset($this->data[static::getPrimaryKey()])) {
            static::getDatabase()->update(static::getTableName(), $this->data, [static::getPrimaryKey() => $this->getPrimaryValue()]);
        } else {
            static::getDatabase()->insert(static::getTableName(), $this->data);
            $this->data[static::getPrimaryKey()] = self::getDatabase()->id();
        }
        $this->data = static::getDatabase()->get(static::getTableName(), '*', [static::getPrimaryKey() => $this->getPrimaryValue()]);
        return $this;
    }

    /**
     * @return void
     */
    public function destroy(): void
    {
        static::getDatabase()->delete(static::getTableName(), [static::getPrimaryKey() => $this->getPrimaryValue()]);
        $this->data = [];
    }

    /**
     * @param array $rawData
     * @return $this
     */
    public function setData(array $rawData): static
    {
        foreach ($rawData as $k => $v)
            $this->$k = $v;
        return $this;
    }

    /**
     * @param array $data
     */
    final public function __construct(private array $data)
    {}

    /**
     * @return int|string
     */
    public function getPrimaryValue(): int|string
    {
        return $this[static::getPrimaryKey()];
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->data;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->data[$name];
    }

    /**
     * @param string $name
     * @param $value
     * @return void
     */
    public function __set(string $name, $value): void
    {
        $this->data[$name] = $value;
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->data[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->$offset;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->$offset = $value;
    }

    /**
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->data[$offset]);
    }

    /**
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->data);
    }
}
