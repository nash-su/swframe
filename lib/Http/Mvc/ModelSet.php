<?php

namespace SWFrame\Http\Mvc;

use JsonSerializable, ArrayAccess, IteratorAggregate, Traversable, ArrayIterator;
use SWFrame\Storage\Database;
use SWFrame\Storage\DatabaseProxy;

class ModelSet implements JsonSerializable, ArrayAccess, IteratorAggregate
{
    /**
     * @var Model[]
     */
    private array $set;

    /**
     * @var string
     */
    private string $modelName;

    /**
     * @param array $set
     * @param string $modelName
     */
    public function __construct(array $set, string $modelName)
    {
        $this->set = $set;
        $this->modelName = $modelName;
    }

    public function getPrimaryKey(): string
    {
        return call_user_func([$this->modelName, 'getPrimaryKey']);
    }

    /**
     * @return array
     */
    public function getPrimaryValues(): array
    {
        $result = [];
        foreach ($this->set as $item)
            $result[] = $item->getPrimaryValue();
        return $result;
    }

    /**
     * @param array $rawData
     * @return ModelSet
     */
    public function save(array $rawData): ModelSet
    {
        $this->getDatabase()->update($this->getTableName(), $rawData, [$this->getPrimaryKey() => $this->getPrimaryValues()]);
        return $this->reload();
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        $this->getDatabase()->delete($this->getTableName(), [$this->getPrimaryKey() => $this->getPrimaryValues()]);
    }

    /**
     * @return int
     */
    public function length(): int
    {
        return count($this->set);
    }

    /**
     * @return ModelSet
     */
    private function reload(): modelSet
    {
        $set = $this->getDatabase()->select($this->getTableName(), '*', [
            $this->getPrimaryKey() => $this->getPrimaryValues(),
            'ORDER' => [$this->getPrimaryKey() => $this->getPrimaryValues()]
        ]);
        $modelName = $this->modelName;
        $this->set = [];
        foreach ($set as $item)
            $this->set[] = new $modelName($item);
        return $this;
    }

    /**
     * @return Database|DatabaseProxy
     */
    public function getDatabase(): Database|DatabaseProxy
    {
        return call_user_func([$this->modelName, 'getDatabase']);
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return call_user_func([$this->modelName, 'getTableName']);
    }

    /**
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->set);
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($offset, $this->set);
    }

    /**
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->set[$offset];
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->set[$offset] = $value;
    }

    /**
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->set[$offset]);
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->set;
    }
}
