<?php

namespace SWFrame\Http\Mvc;

use Swoole\Coroutine;
use Swoole\Http\Request as SwRequest;
use Symfony\Component\HttpFoundation\ParameterBag;

class Request extends \Symfony\Component\HttpFoundation\Request
{
    /**
     * @param array $query
     * @param array $request
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param $content
     */
    private function __construct(array $query = [], array $request = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, [], $cookies, $files, $server, $content);
        $this->request = new ParameterBag($request);
        $this->query = new ParameterBag($query);
        $context = Coroutine::getContext();
        $context[Request::class] = $this;
    }

    /**
     * @var array|string[]
     */
    private static array $mapping = [
        'x-real-ip'       => 'REMOTE_ADDR',
        'x-real-port'     => 'REMOTE_PORT',
        'server-protocol' => 'SERVER_PROTOCOL',
        'server-name'     => 'SERVER_NAME',
        'server-addr'     => 'SERVER_ADDR',
        'server-port'     => 'SERVER_PORT',
        'scheme'          => 'REQUEST_SCHEME',
    ];

    /**
     * @param SwRequest $request
     * @return static
     */
    public static function createFromSWRequest(SwRequest $request): static
    {
        $server = [];
        foreach ($request->server as $k => $v)
            $server[strtoupper($k)] = $v;
        foreach ($request->header as $k => $v)
            $server[self::$mapping[$k] ?? strtoupper('http_' . strtr($k, '-', '_'))] = $v;
        if (isset($server['REQUEST_SCHEME']) && $server['REQUEST_SCHEME'] === 'https')
            $server['HTTPS'] = 'on';
        return new static(
            $request->get??[],
            $request->post??[],
            $request->cookie??[],
            $request->files??[],
            $server,
            $request->getContent() ?? null
        );
    }

    /**
     * @return static
     */
    public static function instance(): self
    {
        return Coroutine::getContext()[Request::class];
    }
}
