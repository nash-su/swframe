<?php

namespace SWFrame\Http\Throwable;

use Exception;

class RouterException extends Exception
{
    public function __construct(int $code = 0)
    {
        parent::__construct('', $code);
    }
}