<?php

namespace SWFrame\Http\Throwable;

use Exception;
use SWFrame\Config;

class HttpException extends Exception
{
    public function __construct(int $code = 0)
    {
        $errors = Config::instance()->get('error');
        $errors = array_column($errors, 1, 0);
        if (array_key_exists($codeIndex = strval($code), $errors))
            parent::__construct($errors[$codeIndex], $code);
        else
            parent::__construct('未知错误', 5000000);
    }
}
