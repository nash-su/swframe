<?php

namespace SWFrame\Http;

use FastRoute\{Dispatcher, RouteCollector};
use ReflectionException;
use ReflectionMethod;
use SWFrame\Http\{Attributes\Route, Mvc\Controller, Mvc\Request, Throwable\RouterException};
use function FastRoute\simpleDispatcher;

class Router
{
    private static self $router;
    private Dispatcher $dispatcher;

    /**
     * @param array $controllerNamespaceList
     * @throws ReflectionException
     */
    private function __construct(array $controllerNamespaceList = [])
    {
        $this->dispatcher = simpleDispatcher(function(RouteCollector $routeCollector) use ($controllerNamespaceList) {
            $this->loadControllers($routeCollector, $controllerNamespaceList);
        });
    }

    /**
     * @param RouteCollector $routeCollector
     * @param array|string $controllerNamespaceListOrNamespace
     * @param string $directory
     * @return void
     * @throws ReflectionException
     */
    protected function loadControllers(RouteCollector $routeCollector, array|string $controllerNamespaceListOrNamespace, string $directory = '')
    {
        if ($directory && !is_dir($directory))
            return;
        if (is_array($controllerNamespaceListOrNamespace)) {
            foreach ($controllerNamespaceListOrNamespace as $namespace => $directory)
                $this->loadControllers($routeCollector, $namespace, $directory);
            return;
        }
        $directory = realpath($directory);
        $files = scandir($directory);
        foreach ($files as $file) {
            if (in_array($file, ['.', '..']))
                continue;
            if (is_dir("$directory/$file")) {
                $this->loadControllers($routeCollector, "$controllerNamespaceListOrNamespace\\$file" , "$directory/$file");
                continue;
            }
            if (!str_ends_with($file, '.php'))
                continue;
            $className = $controllerNamespaceListOrNamespace . '\\' . substr($file, 0, -4);
            if (!class_exists($className) || !is_subclass_of($className, Controller::class))
                continue;
            $this->loadControllerClass($routeCollector, $className);
        }
    }

    /**
     * @param RouteCollector $routeCollector
     * @param string $controllerClassName
     * @return void
     * @throws ReflectionException
     */
    protected function loadControllerClass(RouteCollector $routeCollector, string $controllerClassName)
    {
        $methods = get_class_methods($controllerClassName);
        foreach ($methods as $method) {
            $reflectionMethod = new ReflectionMethod($controllerClassName, $method);
            if (!$reflectionMethod->isPublic() || empty($attributes = $reflectionMethod->getAttributes(Route::class)))
                continue;
            foreach ($attributes as $attribute) {
                $route = $attribute->newInstance();
                if ($route instanceof Route)
                    $route($routeCollector, [$controllerClassName, $method, $reflectionMethod->isStatic()]);
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws RouterException
     */
    public function dispatch(Request $request): array
    {
        $result = $this->dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
        if ($result[0] === Dispatcher::METHOD_NOT_ALLOWED)
            throw new RouterException(405);
        if ($result[0] === Dispatcher::NOT_FOUND)
            throw new RouterException(404);
        return [$result[1], $result[2]];
    }

    /**
     * @param array $controllerNamespaceList
     * @return void
     * @throws ReflectionException
     */
    public static function init(array $controllerNamespaceList = []): void
    {
        empty(Router::$router) && Router::$router = new static($controllerNamespaceList);
    }

    /**
     * @return static
     */
    public static function instance(): self
    {
        return Router::$router;
    }
}
