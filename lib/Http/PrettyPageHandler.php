<?php

namespace SWFrame\Http;

use ArrayAccess, Closure;
use SWFrame\Http\Mvc\Request;
use Whoops\Exception\Formatter;
use Whoops\Handler\Handler;
use Whoops\Handler\PlainTextHandler;

class PrettyPageHandler extends \Whoops\Handler\PrettyPageHandler
{

    public function __construct(
        protected Request $request
    ) {
        parent::__construct();
        $this->handleUnconditionally(true);
    }

    /**
     * @var array[]
     */
    private array $blacklist = [
        '_GET' => [],
        '_POST' => [],
        '_FILES' => [],
        '_COOKIE' => [],
        '_SESSION' => [],
        '_SERVER' => [],
        '_ENV' => [],
    ];

    /**
     * The name of the custom css file.
     *
     * @var string|null
     */
    private ?string $customCss = null;

    /**
     * The name of the custom js file.
     *
     * @var string|null
     */
    private ?string $customJs = null;

    /**
     * @return int|null
     */
    public function handle(): ?int
    {
        $templateFile = $this->getResource("views/layout.html.php");
        $cssFile = $this->getResource("css/whoops.base.css");
        $zeptoFile = $this->getResource("js/zepto.min.js");
        $prismJs = $this->getResource("js/prism.js");
        $prismCss = $this->getResource("css/prism.css");
        $clipboard = $this->getResource("js/clipboard.min.js");
        $jsFile = $this->getResource("js/whoops.base.js");

        if ($this->customCss) {
            $customCssFile = $this->getResource($this->customCss);
        }

        if ($this->customJs) {
            $customJsFile = $this->getResource($this->customJs);
        }

        $inspector = $this->getInspector();
        $frames = $this->getExceptionFrames();
        $code = $this->getExceptionCode();

        // List of variables that will be passed to the layout template.
        $vars = [
            "page_title" => $this->getPageTitle(),

            "stylesheet" => file_get_contents($cssFile),
            "zepto" => file_get_contents($zeptoFile),
            "prismJs" => file_get_contents($prismJs),
            "prismCss" => file_get_contents($prismCss),
            "clipboard" => file_get_contents($clipboard),
            "javascript" => file_get_contents($jsFile),

            // Template paths:
            "header" => $this->getResource("views/header.html.php"),
            "header_outer" => $this->getResource("views/header_outer.html.php"),
            "frame_list" => $this->getResource("views/frame_list.html.php"),
            "frames_description" => $this->getResource("views/frames_description.html.php"),
            "frames_container" => $this->getResource("views/frames_container.html.php"),
            "panel_details" => $this->getResource("views/panel_details.html.php"),
            "panel_details_outer" => $this->getResource("views/panel_details_outer.html.php"),
            "panel_left" => $this->getResource("views/panel_left.html.php"),
            "panel_left_outer" => $this->getResource("views/panel_left_outer.html.php"),
            "frame_code" => $this->getResource("views/frame_code.html.php"),
            "env_details" => $this->getResource("views/env_details.html.php"),

            "title" => $this->getPageTitle(),
            "name" => explode("\\", $inspector->getExceptionName()),
            "message" => $inspector->getExceptionMessage(),
            "previousMessages" => $inspector->getPreviousExceptionMessages(),
            "docref_url" => $inspector->getExceptionDocrefUrl(),
            "code" => $code,
            "previousCodes" => $inspector->getPreviousExceptionCodes(),
            "plain_exception" => Formatter::formatExceptionPlain($inspector),
            "frames" => $frames,
            "has_frames" => !!count($frames),
            "handler" => $this,
            "handlers" => $this->getRun()->getHandlers(),

            "active_frames_tab" => count($frames) && $frames->offsetGet(0)->isApplication() ? 'application' : 'all',
            "has_frames_tabs" => $this->getApplicationPaths(),

            "tables" => [
                "GET Data" => $this->masked($this->request->query->all(), '_GET'),
                "POST Data" => $this->masked($this->request->request->all(), '_POST'),
                "Files" => $this->masked($this->request->files->all(), '_FILES'),
                "Cookies" => $this->masked($this->request->cookies->all(), '_COOKIE'),
                "Server/Request Data" => $this->masked($this->request->server->all(), '_SERVER'),
                "Environment Variables" => $this->masked($_ENV, '_ENV'),
            ],
        ];

        if (isset($customCssFile)) {
            $vars["stylesheet"] .= file_get_contents($customCssFile);
        }

        if (isset($customJsFile)) {
            $vars["javascript"] .= file_get_contents($customJsFile);
        }

        $extraTables = array_map(function ($table) use ($inspector) {
            return $table instanceof Closure ? $table($inspector) : $table;
        }, $this->getDataTables());
        $vars["tables"] = array_merge($extraTables, $vars["tables"]);

        $plainTextHandler = new PlainTextHandler();
        $plainTextHandler->setException($this->getException());
        $plainTextHandler->setInspector($this->getInspector());
        $vars["preface"] = "<!--\n\n\n" . $this->templateHelper->escape($plainTextHandler->generateResponse()) . "\n\n\n\n\n\n\n\n\n\n\n-->";

        $this->templateHelper->setVariables($vars);
        $this->templateHelper->render($templateFile);

        return Handler::QUIT;
    }

    /**
     * @param ArrayAccess|array $superGlobal
     * @param string $superGlobalName
     * @return ArrayAccess|array
     */
    private function masked(ArrayAccess|array $superGlobal, string $superGlobalName): ArrayAccess|array
    {
        $blacklisted = $this->blacklist[$superGlobalName];

        $values = $superGlobal;

        foreach ($blacklisted as $key) {
            if (isset($superGlobal[$key])) {
                $values[$key] = str_repeat('*', is_string($superGlobal[$key]) ? strlen($superGlobal[$key]) : 3);
            }
        }

        return $values;
    }
}
