<?php

namespace SWFrame\Http;

use Auryn\InjectionException;
use Swoole\Http\{Request as SwRequest, Response as SwResponse};
use SWFrame\Http\Mvc\{Request, Response};
use SWFrame\{Config, Injector, Logger};
use Throwable;
use Whoops\Run;
use SWFrame\Http\Throwable\{HttpException, RouterException};

class Event
{
    /**
     * @return void
     */
    public static function onStart(): void
    {
        $config = Server::instance()->getConfig();
        Logger::instance()->info("Http server listen on \"{$config['host']}:{$config['port']}\"");
    }

    /**
     * @param SwRequest $swRequest
     * @param SwResponse $swResponse
     * @return void
     * @throws InjectionException
     */
    public static function onRequest(SwRequest $swRequest, SwResponse $swResponse)
    {
        Request::createFromSWRequest($swRequest);
        $response = Response::createFromSWResponse($swResponse);
        $injector = Injector::instance();
        if (Config::instance()->get('ENV.APP_MODE') === 'debug') {
            $injector->execute([self::class, 'debugRoute']);
        } else {
            try {
                $injector->execute([self::class, 'route']);
            } catch (Throwable $e) {
                $response->setStatusCode(self::getErrorStatusCode($e));
            }
        }
        $response->send();
    }

    /**
     * @param Injector $injector
     * @param Response $response
     * @return void
     * @throws InjectionException
     */
    public static function debugRoute(Injector $injector, Response $response)
    {
        $whoops = new Run();
        $whoops->writeToOutput(false);
        $whoops->allowQuit(false);
        $whoops->appendHandler($injector->make(PrettyPageHandler::class));
        try {
            $injector->execute([self::class, 'route']);
        } catch (Throwable $e) {
            $response->setContent($whoops->handleException($e));
        }
    }

    /**
     * @param Injector $injector
     * @param Request $request
     * @param Response $response
     * @return void
     * @throws InjectionException
     * @throws RouterException
     */
    public static function route(Injector $injector, Request $request, Response $response)
    {
        list(list($controllerName, $methodName, $isStatic), $args) = Router::instance()->dispatch($request);
        try {
            $result = $injector->execute([$isStatic ? $controllerName : $injector->make($controllerName), $methodName], $args);
            if (!is_string($result) && !is_numeric($result))
                $result = ['status' => 200, 'error' => '0', 'messages' => $result];
        } catch (HttpException $e) {
            $result = [
                'status' => intval($e->getCode() / 10000),
                'error' => strval($e->getCode()),
                'messages' => ['error' => $e->getMessage()]
            ];
        }
        if (is_string($result) || is_numeric($result))
            $response->setContent(strval($result));
        else
            $response->setContent(json_encode($result))
                ->headers->set('Content-Type', 'application/json');
    }


    /**
     * @param Throwable $error
     * @return int
     */
    private static function getErrorStatusCode(Throwable $error): int
    {
        if (array_key_exists($code = $error->getCode(), Response::$statusTexts))
            return $code;
        return 500;
    }
}
