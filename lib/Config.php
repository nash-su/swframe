<?php

namespace SWFrame;

use Noodlehaus\Config as BaseConfig;
use Noodlehaus\Parser\Json;
use Noodlehaus\Parser\ParserInterface;

final class Config extends BaseConfig
{
    /**
     * @var Config
     */
    private static Config $_instance;

    /**
     * @param $values
     * @param ParserInterface|null $parser
     * @param bool $string
     */
    private function __construct($values, ParserInterface $parser = null, bool $string = false)
    {
        parent::__construct($values, $parser, $string);
    }

    /**
     * @param array $envArray
     * @return void
     */
    public static function init(array $envArray): void
    {
        if (empty(self::$_instance)) {
            self::$_instance = new Config(json_encode(['ENV' => $envArray]), new Json, true);
            $configDirectory = self::$_instance->get('ENV.APP_CONFIG_DIRECTORY');
            if ($configDirectory[0] !== '/') {
                $configDirectory = ROOT . '/' . $configDirectory;
            }
            $configDirectory = realpath($configDirectory);
            if (is_dir($configDirectory . '/' . self::$_instance->get('ENV.APP_ENV')))
                $configDirectory .= '/' . self::$_instance->get('ENV.APP_ENV');
            $configFiles = scandir($configDirectory);
            foreach ($configFiles as $configFile) {
                if ($configFile == '.' || $configFile == '..')
                    continue;
                self::$_instance->merge(new Config(json_encode([
                    pathinfo($configFile, PATHINFO_FILENAME) =>
                        (new Config($configDirectory . '/' . $configFile))->all()
                ]), new Json, true));
            }
        }
    }

    /**
     * @return static
     */
    public static function instance(): self
    {
        return self::$_instance;
    }
}
