<?php

namespace SWFrame\Attributes;

use Symfony\Component\Console\Command\Command as ConsoleCommand;
use Attribute as BaseAttribute;
use Symfony\Component\Console\Input\InputOption;

#[BaseAttribute(BaseAttribute::TARGET_METHOD|BaseAttribute::IS_REPEATABLE)]
class Option extends Attribute
{
    const VALUE_IS_ARRAY = InputOption::VALUE_IS_ARRAY;
    const VALUE_NEGATABLE = InputOption::VALUE_NEGATABLE;
    const VALUE_NONE = InputOption::VALUE_NONE;
    const VALUE_OPTIONAL = InputOption::VALUE_OPTIONAL;
    const VALUE_REQUIRED = InputOption::VALUE_REQUIRED;

    public function __construct(
        private string $name,
        private array|string|null $shortCut = null,
        private int $mode = self::VALUE_OPTIONAL,
        private string $description = '',
        private mixed $default = null,
    ) {}

    public function __invoke(ConsoleCommand $command)
    {
        $command->addOption($this->name, $this->shortCut, $this->mode, $this->description, $this->default);
    }
}
