<?php

namespace SWFrame\Attributes;

use Symfony\Component\Console\Command\Command as ConsoleCommand;

abstract class Attribute
{
    abstract public function __invoke(ConsoleCommand $command);
}
