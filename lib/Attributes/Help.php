<?php

namespace SWFrame\Attributes;

use Symfony\Component\Console\Command\Command as ConsoleCommand;
use Attribute as BaseAttribute;

#[BaseAttribute(BaseAttribute::TARGET_METHOD)]
class Help extends Attribute
{
    public function __construct(
        private string $help
    ) {}

    public function __invoke(ConsoleCommand $command)
    {
        $command->setHelp($this->help);
    }
}
