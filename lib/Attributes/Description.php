<?php

namespace SWFrame\Attributes;

use Symfony\Component\Console\Command\Command as ConsoleCommand;
use Attribute as BaseAttribute;

#[BaseAttribute(BaseAttribute::TARGET_METHOD)]
class Description extends Attribute
{
    public function __construct(
        private string $description
    ) {}

    public function __invoke(ConsoleCommand $command)
    {
        $command->setDescription($this->description);
    }
}
