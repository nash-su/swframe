<?php

namespace SWFrame\Attributes;

use Attribute as BaseAttribute;
use Symfony\Component\Console\Command\Command as ConsoleCommand;
use Symfony\Component\Console\Input\InputArgument;

#[BaseAttribute(BaseAttribute::TARGET_METHOD|BaseAttribute::IS_REPEATABLE)]
class Argument extends Attribute
{
    const REQUIRED = InputArgument::REQUIRED;
    const OPTIONAL = InputArgument::OPTIONAL;
    const IS_ARRAY = InputArgument::IS_ARRAY;

    public function __construct(
        private string $name,
        private int $mode = self::OPTIONAL,
        private string $description = '',
        private mixed $default = null,
    ) {}

    public function __invoke(ConsoleCommand $command)
    {
        $command->addArgument($this->name, $this->mode, $this->description, $this->default);
    }
}

