<?php

namespace SWFrame\Storage;

use Swoole\ConnectionPool;
use Swoole\Coroutine;

final class Pool
{
    const TYPE_DATABASE = Database::class;
    const TYPE_REDIS = Redis::class;

    /**
     * @var Pool[]
     */
    private static array $pools = [
        Pool::TYPE_DATABASE => [],
        Pool::TYPE_REDIS => []
    ];

    /**
     * @var ConnectionPool
     */
    private ConnectionPool $pool;

    /**
     * @var Database|DatabaseProxy|Redis|null
     */
    private Database|DatabaseProxy|Redis|null $_connection = null;

    /**
     * @param string $poolName
     * @param array $config
     * @param string $type
     */
    public function __construct(private string $poolName, private array $config, private string $type)
    {
        $this->pool = new ConnectionPool(function () {
            return $this->getConnection();
        }, intval($config['size']));
    }

    /**
     * @return Database|DatabaseProxy|Redis
     */
    private function getConnection(): Database|DatabaseProxy|Redis
    {
        return match ($this->type) {
            self::TYPE_DATABASE => $this->getDatabase(),
            self::TYPE_REDIS => $this->getRedis()
        };
    }

    /**
     * @return DatabaseProxy
     */
    private function getDatabase(): DatabaseProxy
    {
        return new DatabaseProxy(function () {
            return new Database($this->config);
        });
    }

    /**
     * @return Redis
     */
    private function getRedis(): Redis
    {
        $redis = new Redis();
        $redis->connect(strval($this->config['host']??'localhost'), intval($this->config['port']??6379));
        if (array_key_exists('password', $this->config) && trim($this->config['password']))
            $redis->auth(strval($this->config['password']));
        if (array_key_exists('database', $this->config) && $this->config['password'])
            $redis->select(intval($this->config['password']));
        return $redis;
    }

    /**
     * @param array $configs
     * @param string $type
     * @return void
     */
    public static function init(array $configs, string $type): void
    {
        foreach ($configs as $poolName => $config) {
            empty(self::$pools[$type][$poolName]) && self::$pools[$type][$poolName] = new Pool($poolName, $config, $type);
        }
    }

    /**
     * @param string $poolName
     * @param string $type
     * @return Pool
     */
    public static function instance(string $poolName, string $type): Pool
    {
        return self::$pools[$type][$poolName];
    }


    /**
     * @return Database|DatabaseProxy|Redis
     */
    public function get(): Database|DatabaseProxy|Redis
    {
        if (Coroutine::getCid() === -1) {
            empty($this->_connection) && $this->_connection = $this->getConnection();
            return $this->_connection;
        }
        $context = Coroutine::getContext();
        if (empty($context[$this->type . '#' . $this->poolName])) {
            $connection = $this->pool->get();
            Coroutine::defer(function () use ($connection) {
                $this->pool->put($connection);
            });
            $context[$this->type . '#' . $this->poolName] = $connection;
        }
        return $context[$this->type . '#' . $this->poolName];
    }
}
