<?php

namespace SWFrame\Storage;

class Redis extends \Redis
{
    public static function init(array $configs)
    {
        Pool::init($configs, Redis::class);
    }

    public static function instance(string $poolName): Redis
    {
        return Pool::instance($poolName, Redis::class)->get();
    }
}
