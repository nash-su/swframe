<?php

namespace SWFrame\Storage;

use Medoo\Medoo;
use PDO;

/**
 * @method array select(string $table, string|array $join, $columns = null, $where = null)
 * @method mixed get(string $table, string|array|null $join = null, $columns = null, $where = null)
 * @method bool has(string $table, $join, $where = null)
 * @method array rand(string $table, $join = null, $columns = null, $where = null)
 * @method int count(string $table, $join = null, $column = null, $where = null)
 * @method string max(string $table, $join, $column = null, $where = null)
 * @method string min(string $table, $join, $column = null, $where = null)
 * @method string avg(string $table, $join, $column = null, $where = null)
 * @method string sum(string $table, $join, $column = null, $where = null)
 */
class Database extends Medoo
{
    public function __construct(array $options)
    {
        $options['error'] = PDO::ERRMODE_EXCEPTION;
        parent::__construct($options);
    }

    /**
     * @param array $configs
     * @return void
     */
    public static function init(array $configs)
    {
        Pool::init($configs, Database::class);
    }

    /**
     * @param string $poolName
     * @return Database|DatabaseProxy
     */
    public static function instance(string $poolName): Database|DatabaseProxy
    {
        return Pool::instance($poolName, Database::class)->get();
    }
}
