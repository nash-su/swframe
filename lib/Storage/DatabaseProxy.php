<?php

namespace SWFrame\Storage;

use Medoo\Medoo;
use PDO, PDOException;
use PDOStatement;
use Swoole\Database\ObjectProxy;

final class DatabaseProxy extends ObjectProxy
{
    public const IO_ERRORS = [
        2002, // MYSQLND_CR_CONNECTION_ERROR
        2006, // MYSQLND_CR_SERVER_GONE_ERROR
        2013, // MYSQLND_CR_SERVER_LOST
    ];
    /** @var Medoo */
    protected $__object;
    /** @var array|null */
    protected ?array $setAttributeContext;
    /** @var callable */
    protected $constructor;
    /** @var int */
    protected int $round = 0;

    /**
     * @param callable $constructor
     */
    public function __construct(callable $constructor)
    {
        parent::__construct($constructor());
        $this->__object->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        $this->constructor = $constructor;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return false|mixed|DatabaseStatementProxy
     */
    public function __call(string $name, array $arguments)
    {
        for ($n = 3; $n--;) {
            $ret = @$this->__object->{$name}(...$arguments);
            if ($ret === false) {
                /* non-IO method */
                $errorInfo = $this->__object->pdo->errorInfo();
                /* no more chances or non-IO failures */
                if (
                    !in_array($errorInfo[1], static::IO_ERRORS, true) || $n === 0
                    || $this->__object->pdo->inTransaction()
                ) {
                    /* '00000' means “no error.”, as specified by ANSI SQL and ODBC. */
                    if (!empty($errorInfo) && $errorInfo[0] !== '00000') {
                        $exception            = new PDOException($errorInfo[2], $errorInfo[1]);
                        $exception->errorInfo = $errorInfo;
                        throw $exception;
                    }
                    /* no error info, just return false */
                    break;
                }
                $this->reconnect();
                continue;
            }
            if ($ret instanceof PDOStatement) {
                $ret = new DatabaseStatementProxy($ret, $this);
            }
            break;
        }
        /* @noinspection PhpUndefinedVariableInspection */
        return $ret;
    }

    /**
     * @return int
     */
    public function getRound(): int
    {
        return $this->round;
    }

    /**
     * @return void
     */
    public function reconnect(): void
    {
        $constructor = $this->constructor;
        parent::__construct($constructor());
        $this->round++;
        /* restore context */
        if ($this->setAttributeContext) {
            foreach ($this->setAttributeContext as $attribute => $value) {
                $this->__object->pdo->setAttribute($attribute, $value);
            }
        }
    }

    /**
     * @param int $attribute
     * @param $value
     * @return bool
     */
    public function setAttribute(int $attribute, $value): bool
    {
        $this->setAttributeContext[$attribute] = $value;
        return $this->__object->pdo->setAttribute($attribute, $value);
    }

    /**
     * @return bool
     */
    public function inTransaction(): bool
    {
        return $this->__object->pdo->inTransaction();
    }
}
