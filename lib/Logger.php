<?php

namespace SWFrame;

use Auryn\InjectionException;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger as Monolog;
use SWFrame\LoggerHandler\ConsoleHandler;
use Symfony\Component\Console\Output\OutputInterface;

final class Logger extends Monolog
{
    /**
     * @var array
     */
    private static array $loggerList = [];

    /**
     * @var Logger
     */
    private static Logger $consoleLogger;

    /**
     * @param array $configs
     * @return void
     * @throws InjectionException
     */
    public static function init(array $configs): void
    {
        empty(self::$consoleLogger) && self::$consoleLogger = (new self('console'))->pushHandler(
            new ConsoleHandler(Injector::instance()->make(OutputInterface::class))
        );
        if (!in_array(Config::instance()->get('ENV.APP_MODE'), ['debug', 'log']))
            return;
        foreach ($configs as $channel => $config) {
            if (is_subclass_of($config['handler'], AbstractProcessingHandler::class))
                empty(self::$loggerList[$channel]) && self::$loggerList[$channel] = (new self($channel))->pushHandler(
                    new ($config['handler'])(...$config['init'])
                );
        }
    }

    /**
     * @param string|null $channel
     * @return Logger
     */
    public static function instance(?string $channel = null): Logger
    {
        if (is_null($channel))
            return self::$consoleLogger;
        return self::$loggerList[$channel];
    }

    /**
     * @param int $level
     * @param string $message
     * @param array $context
     * @return bool
     */
    public function addRecord(int $level, string $message, array $context = []): bool
    {
        if (in_array(Config::instance()->get('ENV.APP_MODE'), ['debug', 'log']))
            return parent::addRecord($level, $message, $context);
        return true;
    }
}
