<?php

namespace SWFrame\Commands;

use Auryn\ConfigException;
use SWFrame\Attributes\{Description, Help, Option};
use SWFrame\{Command, Config, Http\Server, Storage\Database};

class Http extends Command
{
    /**
     * @return void
     * @throws ConfigException
     */
    #[Description('启动服务脚本')]
    #[Help('执行本脚本启动http服务')]
    #[Option(name: 'host', description: '监听地址')]
    #[Option(name: 'port', description: '监听端口')]
    public static function start(): void
    {
        Server::init(Config::instance()->get('http'));
        Server::instance()->start();
    }
}
