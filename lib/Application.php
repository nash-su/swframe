<?php

namespace SWFrame;

use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Application extends ConsoleApplication
{
    const APP_NAME = 'SWFrame';
    const APP_VERSION = 'v1.0';

    /**
     * @var Application
     */
    private static Application $_instance;

    /**
     * @var InputInterface
     */
    public static InputInterface $input;

    /**
     * @var OutputInterface
     */
    public static OutputInterface $output;

    /**
     * @param array $commandPsr4
     */
    public function __construct(array $commandPsr4)
    {
        parent::__construct(self::APP_NAME, self::APP_VERSION);
        $this->loadCommands($commandPsr4);
    }

    /**
     * @param array|string $commandNamespaceListOrNamespace
     * @param string $directory
     * @return void
     */
    protected function loadCommands(array|string $commandNamespaceListOrNamespace, string $directory = '')
    {
        if ($directory && !is_dir($directory))
            return;
        if (is_array($commandNamespaceListOrNamespace)) {
            foreach ($commandNamespaceListOrNamespace as $namespace => $directory)
                $this->loadCommands($namespace, $directory);
            return;
        }
        $directory = realpath($directory);
        $files = scandir($directory);
        foreach ($files as $file) {
            if (in_array($file, ['.', '..']) || is_dir("$directory/$file") || !str_ends_with($file, '.php'))
                continue;
            $className = $commandNamespaceListOrNamespace . '\\' . substr($file, 0, -4);
            if (!class_exists($className) || !is_subclass_of($className, Command::class))
                continue;
            $this->loadCommandClass($className, $file);
        }
    }

    /**
     * @param string $commandClassName
     * @param string $commandFileName
     * @return void
     */
    protected function loadCommandClass(string $commandClassName, string $commandFileName)
    {
        $methods = get_class_methods($commandClassName);
        foreach ($methods as $method) {
            if ('makeCommand' !== $method && is_callable([$commandClassName, $method]))
                $this->add(Command::makeCommand($commandClassName, $method, substr($commandFileName, 0, -4)));
        }
    }

    /**
     * @param array $commandPsr4
     * @return void
     */
    public static function init(array $commandPsr4 = []): void
    {
        empty(self::$_instance) && Application::$_instance = new Application($commandPsr4);
    }

    /**
     * @return static
     */
    public static function instance(): self
    {
        return Application::$_instance;
    }
}
