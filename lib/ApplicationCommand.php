<?php

namespace SWFrame;

use ReflectionException, ReflectionMethod, Throwable;
use SWFrame\Attributes\Attribute;
use SWFrame\Storage\{Database, Redis};
use Symfony\Component\Console\{Input\InputInterface, Output\OutputInterface, Command\Command as ConsoleCommand};

final class ApplicationCommand extends ConsoleCommand
{
    /**
     * @param string $name
     * @param $executeMethod
     */
    public function __construct(string $name, protected $executeMethod)
    {
        parent::__construct($name);
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    protected function configure()
    {
        $attributes = (new ReflectionMethod(...$this->executeMethod))->getAttributes();
        foreach ($attributes as $attribute) {
            if (!class_exists($attribute->getName()))
                continue;
            $attributeConfig = $attribute->newInstance();
            if (is_subclass_of($attributeConfig, Attribute::class)) {
                $attributeConfig($this);
            }
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $injector = Injector::instance()
                ->share($input)->alias(InputInterface::class, get_class($input))
                ->share($output)->alias(OutputInterface::class, get_class($output));
            $injector->delegate(Application::class, [Application::class, 'instance']);
            $injector->delegate(Config::class, [Config::class, 'instance']);
            Logger::init(Config::instance()->get('log'));
            Database::init(Config::instance()->get('database'));
            Redis::init(Config::instance()->get('redis'));
            return intval($injector->execute($this->executeMethod) ?? ConsoleCommand::SUCCESS);
        } catch (Throwable $e) {
            echo $e->getTraceAsString(), PHP_EOL;
            return max(1, $e->getCode());
        }
    }
}
