<?php

namespace SWFrame\LoggerHandler;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Symfony\Component\Console\Output\OutputInterface;

class ConsoleHandler extends AbstractProcessingHandler
{

    public function __construct(private ?OutputInterface $output, $level = Logger::DEBUG, bool $bubble = true)
    {
        parent::__construct($level, $bubble);
    }

    protected function write(array $record): void
    {
        if (is_null($this->output)) {
            echo $record['formatted'];
        } else {
            $this->output->write($record['formatted']);
        }
    }
}
