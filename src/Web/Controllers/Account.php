<?php

namespace App\Web\Controllers;

use SWFrame\Http\Attributes\Route;
use SWFrame\Http\Mvc\Controller;
use SWFrame\Http\Mvc\Request;

class Account extends Controller
{
    #[Route('GET', '/auth/wx-login')]
    public function loginWithWechatMiniApp(Request $request): array
    {
        $id = $request->query->get('id');
        $account = \App\Web\Models\Account::search(['id' => $id]);
        return ['result' => $account->length()];
    }

}
