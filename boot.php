<?php

const ROOT = __DIR__;

SWFrame\Config::init(Dotenv\Dotenv::parse(file_get_contents(ROOT . '/.env')));

date_default_timezone_set(SWFrame\Config::instance()->get('ENV.APP_TIMEZONE'));

SWFrame\Application::init(SWFrame\Config::instance()->get('application.psr-4'));

SWFrame\Application::instance()->run();

