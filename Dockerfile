FROM openswoole/swoole:4.10-php8.1-alpine

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories

# 安装composer并设置使用阿里云作为镜像源
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/bin/composer \
    && chmod +x /usr/bin/composer \
    && composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/

RUN wget https://pecl.php.net/get/redis-5.3.7.tgz \
    && mkdir -p /usr/src/php/ext \
    && tar zxf redis-5.3.7.tgz -C /usr/src/php/ext \
    && mv /usr/src/php/ext/redis-5.3.7 /usr/src/php/ext/redis \
    && rm -r redis-5.3.7.tgz

RUN docker-php-ext-install -j$(nproc) pdo pdo_mysql redis bcmath

# 设置工作目录
WORKDIR /data

# 设置默认的启动点
ENTRYPOINT ["php", "/data/bin/app"]
